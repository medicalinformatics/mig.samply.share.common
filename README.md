# Samply.Share.Common
> Commonly used methods for samply.share.client and samply.share.broker

This library contains methods mostly to display and manipulate queries that are created and distributed
by samply.share.broker and downloaded/displayed by samply.share.client.

## Build

Use maven to build the jar:

```
mvn clean package
```

Use it as a dependency:

```xml
<dependency>
    <groupId>de.mig.samply</groupId>
    <artifactId>share-common</artifactId>
    <version>VERSION</version>
</dependency>
```