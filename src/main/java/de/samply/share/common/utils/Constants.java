/*
 * Copyright (C) 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.share.common.utils;

/**
 * Holds shared constants that are used in broker and client
 */
public class Constants {

  public static final String ACCESSTOKEN_HEADER_KEY = "Accesstoken";
  public static final String AUTH_HEADER_VALUE_REGISTRATION = "SamplyRegistration";
  public static final String AUTH_HEADER_VALUE_SAMPLY = "Samply";
  public static final String BANKS_PATH = "rest/searchbroker/banks/";
  public static final String CONTACT_PATH = "contact";
  public static final String EXPOSE_CHECK_PATH = "hasexpose";
  public static final String EXPOSE_UNAVAILABLE = "unavailable";
  public static final String INFO_PATH = "info";
  public static final String INQUIRIES_PATH = "rest/searchbroker/inquiries";
  public static final String TESTINQUIRIES_PATH = "rest/test/inquiries";
  public static final String REGISTRATION_AUTH_HEADER_VALUE = "SamplyRegistration AuthCode";
  public static final String REGISTRATION_HEADER_VALUE = "SamplyRegistration ";
  public static final String REPLIES_PATH = "replies";
  public static final String SERVER_HEADER_KEY = "Server";
  public static final String SERVER_HEADER_VALUE_PREFIX = "Samply.Share.Broker/";
  public static final String MONITORING_PATH = "rest/monitoring";
  public static final String REFERENCEQUERY_PATH = MONITORING_PATH + "/referencequery";
  public static final String HEADER_XML_NAMESPACE = "samply-xml-namespace";
  public static final String VALUE_XML_NAMESPACE_COMMON = "common";
  public static final String VALUE_XML_NAMESPACE_CCP = "ccp";
  public static final String VALUE_XML_NAMESPACE_OSSE = "osse";
  public static final String VALUE_XML_NAMESPACE_BBMRI = "bbmri";
  public static final String VALUE_XML_NAMESPACE_GBA = "gba";

  public static final String INQUIRY_REPLY_TECHNICAL_ERROR = "ERROR_TECHNICAL";
  public static final String INQUIRY_REPLY_NOT_REPLYING_ON_PURPOSE = "WONT_REPLY";
  public static final String INQUIRY_REPLY_WAITING_FOR_LOCAL_DECISION = "PENDING";

}
