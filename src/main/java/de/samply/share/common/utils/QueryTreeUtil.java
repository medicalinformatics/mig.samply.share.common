/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.share.common.utils;

import de.samply.share.common.model.uiquerybuilder.EnumConjunction;
import de.samply.share.common.model.uiquerybuilder.EnumOperator;
import de.samply.share.common.model.uiquerybuilder.QueryItem;
import de.samply.share.model.query.common.And;
import de.samply.share.model.query.common.Attribute;
import de.samply.share.model.query.common.Between;
import de.samply.share.model.query.common.ConditionType;
import de.samply.share.model.query.common.Eq;
import de.samply.share.model.query.common.Geq;
import de.samply.share.model.query.common.Gt;
import de.samply.share.model.query.common.In;
import de.samply.share.model.query.common.IsNotNull;
import de.samply.share.model.query.common.IsNull;
import de.samply.share.model.query.common.Leq;
import de.samply.share.model.query.common.Like;
import de.samply.share.model.query.common.Lt;
import de.samply.share.model.query.common.MultivalueAttribute;
import de.samply.share.model.query.common.Neq;
import de.samply.share.model.query.common.ObjectFactory;
import de.samply.share.model.query.common.Or;
import de.samply.share.model.query.common.Query;
import de.samply.share.model.query.common.RangeAttribute;
import de.samply.share.model.query.common.Where;
import de.samply.share.utils.QueryConverter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import org.omnifaces.model.tree.ListTreeModel;
import org.omnifaces.model.tree.TreeModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Offer Methods to help with query to tree conversion and vice versa
 */
public class QueryTreeUtil {

  /**
   * The Constant logger.
   */
  private static final Logger logger = LoggerFactory.getLogger(QueryTreeUtil.class);

  /**
   * The Constant CONJUNCTION_GROUP.
   */
  private static final String CONJUNCTION_GROUP = "conjunctionGroup";

  /**
   * Prohibit class instantiation since it only offers static methods
   */
  private QueryTreeUtil() {
  }

  /**
   * Take a query criteria string (most likely from the database) and transform it into a tree to
   * show on a jsf page
   *
   * As of now, this uses the OSSE namespace
   *
   * @param queryString the serialized query
   * @return the tree model of the query
   */
  public static TreeModel<QueryItem> queryStringToTree(String queryString) {
    Query query = new Query();
    Where where = new Where();
    query.setWhere(where);

    if (queryString == null || queryString.length() < 1) {
      logger.error("Error...empty query String was submitted...");
      return queryToTree(query);
    }
    try {
      query = SamplyShareUtils.unmarshal(queryString,
          JAXBContext.newInstance(Query.class),
          Query.class);
    } catch (JAXBException e) {
      logger.debug("JAXB Error while converting querystring to tree. Returning empty query");
    }
    return queryToTree(query);
  }

  /**
   * Take a query object and transform it into a tree to show on a jsf page
   *
   * As of now, this uses the OSSE namespace
   *
   * @param query the query
   * @return the tree model of the query
   */
  public static TreeModel<QueryItem> queryToTree(Query query) {
    TreeModel<QueryItem> criteriaTree = new ListTreeModel<>();

    Where where = query.getWhere();
    if (where != null) {
      for (int i = 0; i < where.getAndOrEqOrLike().size(); ++i) {
        criteriaTree = visitNode(criteriaTree, where.getAndOrEqOrLike().get(i));
      }
    }
    return criteriaTree;
  }

  /**
   * Check if the queryTree has at least one Attribute set
   *
   * @param node the root node of the query tree
   * @return true if the queryTree contains at least one Attribute, false otherwise
   */
  public static boolean isQueryValid(TreeModel<QueryItem> node) {
    if (node.isLeaf()) {
      String mdrId = node.getData().getMdrId();
      return mdrId != null && mdrId.contains(":dataelement:");
    } else {
      boolean hasValidChild = false;
      for (TreeModel<QueryItem> child : node.getChildren()) {
        if (isQueryValid(child)) {
          hasValidChild = true;
        }
      }
      return hasValidChild;
    }
  }

  /**
   * Transform a query tree to a query object
   *
   * As of now, this uses the OSSE namespace
   *
   * @param queryTree the tree model, containing the query
   * @return the query itself
   */
  public static Query treeToQuery(TreeModel<QueryItem> queryTree) {
    Query query = new Query();
    Where where = new Where();

    where = (Where) visitNodeAndAppendToQuery(queryTree, where);
    query.setWhere(where);
    try {
      logger.debug(QueryConverter.queryToXml(query));
    } catch (JAXBException e) {
      logger.warn("Error caught while trying to serialize query. Returning empty query", e);
    }

    return query;
  }

  /**
   * Transform a serialized condition (and/or) object to a treenode
   *
   * @param conditionTypeString serialized form of the condition type
   * @return the treenode representation of the condition
   */
  public static TreeModel<QueryItem> conditionTypeStringToTreenode(String conditionTypeString) {
    if (conditionTypeString == null || conditionTypeString.length() < 1) {
      logger.warn("Condition Type String is empty or null...bailing out");
      return new ListTreeModel<>();
    }

    try {
      And and = SamplyShareUtils.unmarshal(conditionTypeString,
          JAXBContext.newInstance(And.class),
          And.class);
      return conditionTypeToTreeNode(and);
    } catch (JAXBException e) {
      logger.debug("JAXB Error while trying to unmarshall as AND. Trying OR now...");
      try {
        Or or = SamplyShareUtils.unmarshal(conditionTypeString,
            JAXBContext.newInstance(Or.class),
            Or.class);
        return conditionTypeToTreeNode(or);
      } catch (JAXBException e1) {
        logger
            .error("JAXB Error: Could not convert String to ConditionType: " + conditionTypeString);
      }
    }
    return new ListTreeModel<>();
  }

  /**
   * Transform a condition (and/or) object to a treenode
   *
   * @param conditionType condition type object
   * @return the treenode representation of the condition
   */
  public static TreeModel<QueryItem> conditionTypeToTreeNode(ConditionType conditionType) {
    TreeModel<QueryItem> treeNode = new ListTreeModel<>();
    TreeModel<QueryItem> newNode;

    EnumConjunction conjunctionType;

    if (conditionType.getClass() == And.class) {
      conjunctionType = EnumConjunction.AND;
    } else if (conditionType.getClass() == Or.class) {
      conjunctionType = EnumConjunction.OR;
    } else {
      // This should never be reached
      conjunctionType = EnumConjunction.AND;
    }

    QueryItem qi = new QueryItem();
    qi.setConjunction(conjunctionType);
    qi.setMdrId("conjunctionGroup");
    qi.setRoot(false);
    newNode = treeNode.addChild(qi);
    for (int i = 0; i < conditionType.getAndOrEqOrLike().size(); ++i) {
      newNode = visitNode(newNode, conditionType.getAndOrEqOrLike().get(i));
    }
    return treeNode;
  }

  /**
   * Check a tree node and add its data to the query
   *
   * @param node the treenode to visit
   * @param target the condition object where the content of the treenode will be appended
   * @return the condition object with appended new data
   */
  private static Object visitNodeAndAppendToQuery(TreeModel<QueryItem> node, ConditionType target) {
    if (node.isRoot()) {
      logger.trace("Visiting root");
      Where where = (Where) target;
      for (TreeModel<QueryItem> queryItem : node.getChildren()) {
        where = (Where) visitNodeAndAppendToQuery(queryItem, where);
      }
      return target;
    } else {
      logger.trace("visiting " + node.getIndex() + " - target: " + target.getClass());

      if (node.isLeaf()) {

        if (target.getClass() == Where.class) {
          Where where = new Where();
          for (TreeModel<QueryItem> qi : node.getChildren()) {
            where = (Where) visitNodeAndAppendToQuery(qi, where);
          }
          return target;
        }

        ObjectFactory objectFactory = new ObjectFactory();
        QueryItem queryItem = node.getData();
        String operatorString = "";
        if (queryItem.getOperator() == null) {
          logger.trace("operator is null");
        } else {
          operatorString = queryItem.getOperator().toString();
        }
        Attribute attribute = new Attribute();
        attribute.setMdrKey(queryItem.getMdrId());

        String dateString = queryItem.getValue();
        if (dateString != null) {
          try {
            dateString = SamplyShareUtils.convertDateStringToString(queryItem.getValue(),
                new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH),
                new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH));
          } catch (ParseException e) {
            // ParseException can be safely ignored here (meaning it is just not a date value)
          }
        }
        JAXBElement<String> jaxbElement = objectFactory.createValue(dateString);
        attribute.setValue(jaxbElement);

        if (operatorString.equalsIgnoreCase(EnumOperator.EQUAL.toString())) {
          Eq eq = new Eq();
          eq.setAttribute(attribute);
          target.getAndOrEqOrLike().add(eq);
        } else if (operatorString.equalsIgnoreCase(EnumOperator.GREATER.toString())) {
          Gt gt = new Gt();
          gt.setAttribute(attribute);
          target.getAndOrEqOrLike().add(gt);
        } else if (operatorString.equalsIgnoreCase(EnumOperator.GREATER_OR_EQUAL.toString())) {
          Geq geq = new Geq();
          geq.setAttribute(attribute);
          target.getAndOrEqOrLike().add(geq);
        } else if (operatorString.equalsIgnoreCase(EnumOperator.LESS_THEN.toString())) {
          Lt lt = new Lt();
          lt.setAttribute(attribute);
          target.getAndOrEqOrLike().add(lt);
        } else if (operatorString.equalsIgnoreCase(EnumOperator.LESS_OR_EQUAL_THEN.toString())) {
          Leq leq = new Leq();
          leq.setAttribute(attribute);
          target.getAndOrEqOrLike().add(leq);
        } else if (operatorString.equalsIgnoreCase(EnumOperator.LIKE.toString())) {
          Like like = new Like();
          // Check if the value is enclosed in wildcard characters. If not, add them.
          // Currently, queries coming from central search do NOT contain wildcards. If they change that, this should handle it.
          String oldValue = attribute.getValue().getValue();
          StringBuilder newValue = new StringBuilder();
          if (!oldValue.startsWith("%")) {
            newValue.append("%");
          }
          newValue.append(oldValue);
          if (!oldValue.endsWith("%")) {
            newValue.append("%");
          }
          attribute.setValue(objectFactory.createValue(newValue.toString()));
          like.setAttribute(attribute);
          target.getAndOrEqOrLike().add(like);
        } else if (operatorString.equalsIgnoreCase(EnumOperator.NOT_EQUAL_TO.toString())) {
          Neq neq = new Neq();
          neq.setAttribute(attribute);
          target.getAndOrEqOrLike().add(neq);
        } else if (operatorString.equalsIgnoreCase(EnumOperator.IS_NOT_NULL.toString())) {
          IsNotNull inn = new IsNotNull();
          inn.setMdrKey(attribute.getMdrKey());
          target.getAndOrEqOrLike().add(inn);
        } else if (operatorString.equalsIgnoreCase(EnumOperator.IS_NULL.toString())) {
          IsNull in = new IsNull();
          in.setMdrKey(attribute.getMdrKey());
          target.getAndOrEqOrLike().add(in);
        } else if (operatorString.equalsIgnoreCase(EnumOperator.BETWEEN.toString())) {
          Between between = new Between();
          RangeAttribute rangeAttr = new RangeAttribute();
          rangeAttr.setMdrKey(queryItem.getMdrId());

          String lowerBoundString = queryItem.getLowerBound();
          try {
            lowerBoundString = SamplyShareUtils.convertDateStringToString(queryItem.getLowerBound(),
                new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH),
                new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH));
          } catch (ParseException e) {
            // Parse Exception can be safely ignored here
          }

          String upperBoundString = queryItem.getUpperBound();
          try {
            upperBoundString = SamplyShareUtils.convertDateStringToString(queryItem.getUpperBound(),
                new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH),
                new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH));
          } catch (ParseException e) {
            // Parse Exception can be safely ignored here
          }
          rangeAttr.setLowerBound(lowerBoundString);
          rangeAttr.setUpperBound(upperBoundString);

          between.setRangeAttribute(rangeAttr);

          target.getAndOrEqOrLike().add(between);
        } else if (operatorString.equalsIgnoreCase(EnumOperator.IN.toString())) {
          In in = new In();
          MultivalueAttribute mvAttr = new MultivalueAttribute();
          mvAttr.setMdrKey(queryItem.getMdrId());

          for (String value : queryItem.getValues()) {
            jaxbElement = objectFactory.createValue(value);
            mvAttr.getValue().add(jaxbElement);
          }

          in.setMultivalueAttribute(mvAttr);
          target.getAndOrEqOrLike().add(in);
        }

        return target;
      } else if (node.getData().getMdrId().equalsIgnoreCase(CONJUNCTION_GROUP)) {
        QueryItem queryItem = node.getData();

        if (queryItem.getConjunction().equals(EnumConjunction.OR)) {
          Or or = new Or();
          for (TreeModel<QueryItem> qi : node.getChildren()) {
            or = (Or) visitNodeAndAppendToQuery(qi, or);
          }
          target.getAndOrEqOrLike().add(or);
        } else {
          // Use AND as default as well
          And and = new And();
          for (TreeModel<QueryItem> qi : node.getChildren()) {
            and = (And) visitNodeAndAppendToQuery(qi, and);
          }
          target.getAndOrEqOrLike().add(and);
        }

        return target;
      }
    }
    logger.error("This should never be reached");
    return null;
  }

  /**
   * Check an entity of a query object and add it to the treenode
   *
   * @param parentNode the node the new item will be attached to
   * @param newItem the item to attach
   * @return the parentNode with the new item attached
   */
  private static TreeModel<QueryItem> visitNode(TreeModel<QueryItem> parentNode, Object newItem) {

    QueryItem queryItem = new QueryItem();
    TreeModel<QueryItem> newNode;

    if (newItem.getClass() == And.class) {
      queryItem.setConjunction(EnumConjunction.AND);
      queryItem.setMdrId("conjunctionGroup");
      queryItem.setRoot(parentNode.isRoot());
      newNode = parentNode.addChild(queryItem);
      for (int j = 0; j < ((And) newItem).getAndOrEqOrLike().size(); ++j) {
        newNode = visitNode(newNode, ((And) newItem).getAndOrEqOrLike().get(j));
      }
    } else if (newItem.getClass() == Or.class) {
      queryItem.setConjunction(EnumConjunction.OR);
      queryItem.setMdrId("conjunctionGroup");
      queryItem.setRoot(parentNode.isRoot());
      newNode = parentNode.addChild(queryItem);
      for (int k = 0; k < ((Or) newItem).getAndOrEqOrLike().size(); ++k) {
        newNode = visitNode(newNode, ((Or) newItem).getAndOrEqOrLike().get(k));
      }
    } else if (newItem.getClass() == Eq.class) {
      Eq eq = (Eq) newItem;
      queryItem.setMdrId(eq.getAttribute().getMdrKey());
      queryItem.setValue(eq.getAttribute().getValue().getValue());
      queryItem.setOperator(EnumOperator.EQUAL);
      parentNode.addChild(queryItem);

    } else if (newItem.getClass() == Geq.class) {
      Geq geq = (Geq) newItem;
      queryItem.setMdrId(geq.getAttribute().getMdrKey());
      queryItem.setValue(geq.getAttribute().getValue().getValue());
      queryItem.setOperator(EnumOperator.GREATER_OR_EQUAL);
      parentNode.addChild(queryItem);

    } else if (newItem.getClass() == Leq.class) {
      Leq leq = (Leq) newItem;
      queryItem.setMdrId(leq.getAttribute().getMdrKey());
      queryItem.setValue(leq.getAttribute().getValue().getValue());
      queryItem.setOperator(EnumOperator.LESS_OR_EQUAL_THEN);
      parentNode.addChild(queryItem);

    } else if (newItem.getClass() == Gt.class) {
      Gt gt = (Gt) newItem;
      queryItem.setMdrId(gt.getAttribute().getMdrKey());
      queryItem.setValue(gt.getAttribute().getValue().getValue());
      queryItem.setOperator(EnumOperator.GREATER);
      parentNode.addChild(queryItem);

    } else if (newItem.getClass() == Lt.class) {
      Lt lt = (Lt) newItem;
      queryItem.setMdrId(lt.getAttribute().getMdrKey());
      queryItem.setValue(lt.getAttribute().getValue().getValue());
      queryItem.setOperator(EnumOperator.LESS_THEN);
      parentNode.addChild(queryItem);

    } else if (newItem.getClass() == Like.class) {
      Like like = (Like) newItem;
      queryItem.setMdrId(like.getAttribute().getMdrKey());
      String val = like.getAttribute().getValue().getValue();
      if (val != null && val.length() > 2 && val.startsWith("%") && val.endsWith("%")) {
        val = val.substring(1, val.length() - 1);
      }
      queryItem.setValue(val);
      queryItem.setOperator(EnumOperator.LIKE);
      parentNode.addChild(queryItem);

    } else if (newItem.getClass() == Neq.class) {
      Neq neq = (Neq) newItem;
      queryItem.setMdrId(neq.getAttribute().getMdrKey());
      queryItem.setValue(neq.getAttribute().getValue().getValue());
      queryItem.setOperator(EnumOperator.NOT_EQUAL_TO);
      parentNode.addChild(queryItem);

    } else if (newItem.getClass() == IsNull.class) {
      IsNull isNull = (IsNull) newItem;
      queryItem.setMdrId(isNull.getMdrKey());
      queryItem.setOperator(EnumOperator.IS_NULL);
      parentNode.addChild(queryItem);

    } else if (newItem.getClass() == IsNotNull.class) {
      IsNotNull isNotNull = (IsNotNull) newItem;
      queryItem.setMdrId(isNotNull.getMdrKey());
      queryItem.setOperator(EnumOperator.IS_NOT_NULL);
      parentNode.addChild(queryItem);

    } else if (newItem.getClass() == Between.class) {
      Between between = (Between) newItem;
      queryItem.setMdrId(between.getRangeAttribute().getMdrKey());
      queryItem.setLowerBound(between.getRangeAttribute().getLowerBound());
      queryItem.setUpperBound(between.getRangeAttribute().getUpperBound());
      queryItem.setOperator(EnumOperator.BETWEEN);
      parentNode.addChild(queryItem);

    } else if (newItem.getClass() == In.class) {
      In in = (In) newItem;
      queryItem.setMdrId(in.getMultivalueAttribute().getMdrKey());
      queryItem.setIn(in.getMultivalueAttribute());
      queryItem.setOperator(EnumOperator.IN);
      parentNode.addChild(queryItem);

    }
    return parentNode;
  }


}
