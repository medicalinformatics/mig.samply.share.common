/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.share.common.utils;

import java.io.IOException;
import java.util.Properties;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

/**
 * This class provides a single instance encapsulating information about the project. It is
 * initialized by the AbstractStartupListener
 */
public class ProjectInfo {

  public static final ProjectInfo INSTANCE = new ProjectInfo();

  /**
   * The project name.
   */
  private String projectName;

  /**
   * The version string.
   */
  private String versionString;

  /**
   * Build date string
   */
  private String buildDateString;

  /**
   * The context.
   */
  private ServletContext context;

  /**
   * The configuration of the project.
   */
  private AbstractConfig config;

  /**
   * Gets the servlet context.
   *
   * @return the servlet context
   */
  public ServletContext getServletContext() {
    return context;
  }

  /**
   * Gets the project name.
   *
   * @return the project name
   */
  public String getProjectName() {
    return projectName;
  }

  public void setProjectName(String projectName) {
    this.projectName = projectName;
  }

  /**
   * Gets the version of the project as string.
   *
   * @return the version string
   */
  public String getVersionString() {
    return versionString;
  }

  public void setVersionString(String versionString) {
    this.versionString = versionString;
  }

  public void setContext(ServletContext context) {
    this.context = context;
  }

  public AbstractConfig getConfig() {
    return config;
  }

  public void setConfig(AbstractConfig config) {
    this.config = config;
  }

  public String getBuildDateString() {
    return buildDateString;
  }

  public void setBuildDateString(String buildDateString) {
    this.buildDateString = buildDateString;
  }

  /**
   * Reads project name and version from manifest file.
   *
   * @param sce the ServletContextEvent from startup
   */
  public void initProjectMetadata(ServletContextEvent sce) {
    context = sce.getServletContext();
    Properties prop = new Properties();
    try {
      prop.load(context.getResourceAsStream("/META-INF/MANIFEST.MF"));
    } catch (IOException ex) {
      throw new RuntimeException("Manifest file not found", ex);
    }
    projectName = prop.getProperty("Samply-Project-Context");
    versionString = prop.getProperty("Implementation-Version");
    buildDateString = prop.getProperty("Build-Timestamp");
  }

}
