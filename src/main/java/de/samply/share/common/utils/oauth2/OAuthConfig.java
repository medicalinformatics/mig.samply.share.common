/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.share.common.utils.oauth2;

import de.samply.auth.rest.Scope;
import de.samply.auth.utils.OAuth2ClientConfig;
import de.samply.common.config.OAuth2Client;
import de.samply.common.config.ObjectFactory;
import de.samply.config.util.JAXBUtil;
import de.samply.share.common.utils.ProjectInfo;
import de.samply.share.common.utils.SamplyShareUtils;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

public class OAuthConfig {

  /**
   * The xhtml loaded on OAuth server redirect.
   */
  protected static final String SAMPLY_OAUTH_DEFAULT_REDIRECT_URI = "/user/dashboard.xhtml";
  /**
   * The xhtml loaded on OAuth server redirect.
   */
  protected static final String LOCAL_LOGOUT_REDIRECT_XHTML = "/index.xhtml";
  private static final String CONTEXT_DEFAULT = "samply";
  private static final String CONFIG_FILENAME_DEFAULT = "OAuth2Client.xml";
  private static final Logger logger = LoggerFactory.getLogger(OAuthConfig.class);

  public static String getAuthLoginUrl() throws UnsupportedEncodingException {
    return getAuthLoginUrl(null, CONFIG_FILENAME_DEFAULT);
  }

  /**
   * Get the url to be called for user authentication - from the authentication server. E.g. URL
   * called when the user clicks on a "Login with Samply account" button.
   *
   * @return the URL to be called for user authentication
   */
  public static String getAuthLoginUrl(String state, String configFilename)
      throws UnsupportedEncodingException {
    ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();

    String requestedPage = context.getRequestServletPath();
    if (SamplyShareUtils.isNullOrEmpty(requestedPage) || requestedPage.contains("index")
        || requestedPage.contains("loginRedirect")) {
      return OAuth2ClientConfig.getRedirectUrl(getOAuth2Client(CONTEXT_DEFAULT, configFilename),
          context.getRequestScheme(), context.getRequestServerName(),
          context.getRequestServerPort(),
          context.getRequestContextPath(), SAMPLY_OAUTH_DEFAULT_REDIRECT_URI, null, state,
          Scope.OPENID);
    } else {
      return OAuth2ClientConfig.getRedirectUrl(getOAuth2Client(CONTEXT_DEFAULT, configFilename),
          context.getRequestScheme(), context.getRequestServerName(),
          context.getRequestServerPort(),
          context.getRequestContextPath(), requestedPage, null, state, Scope.OPENID);
    }
  }

  /**
   * Get the url to be called for user authentication - from the authentication server. E.g. URL
   * called when the user clicks on a "logout" button.
   *
   * @return the URL to be called for user log out in the authentication server
   */
  public static String getAuthLogoutUrl(String configFilename) throws UnsupportedEncodingException {
    return getAuthLogoutUrl(configFilename, null);
  }

  /**
   * Get the url to be called for user authentication - from the authentication server. E.g. URL
   * called when the user clicks on a "logout" button.
   *
   * @return the URL to be called for user log out in the authentication server
   */
  public static String getAuthLogoutUrl(String configFilename, String externalIpLogoutUrl)
      throws UnsupportedEncodingException {
    String projectName = ProjectInfo.INSTANCE.getProjectName();
    ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();

    if (SamplyShareUtils.isNullOrEmpty(externalIpLogoutUrl)) {
      return OAuth2ClientConfig.getLogoutUrl(getOAuth2Client(CONTEXT_DEFAULT, configFilename),
          context.getRequestScheme(), context.getRequestServerName(),
          context.getRequestServerPort(), context.getRequestContextPath(),
          LOCAL_LOGOUT_REDIRECT_XHTML);
    } else {
      return OAuth2ClientConfig.getLogoutUrl(getOAuth2Client(CONTEXT_DEFAULT, configFilename),
          context.getRequestScheme(), context.getRequestServerName(),
          context.getRequestServerPort(), context.getRequestContextPath(),
          "/externalLogout.xhtml?url=" + URLEncoder
              .encode(externalIpLogoutUrl, StandardCharsets.UTF_8.displayName()));
    }
  }

  /**
   * Get the {@link OAuth2Client} instance
   *
   * @return an instance of the OAuth2Client
   */
  public static OAuth2Client getOAuth2Client() {
    return getOAuth2Client(CONTEXT_DEFAULT, CONFIG_FILENAME_DEFAULT);
  }

  public static OAuth2Client getOAuth2Client(String projectContext) {
    return getOAuth2Client(projectContext, CONFIG_FILENAME_DEFAULT);
  }

  public static OAuth2Client getOAuth2Client(String projectContext, String configFilename) {
    OAuth2Client oauthClient = null;
    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
      oauthClient = JAXBUtil
          .findUnmarshall(configFilename, jaxbContext, OAuth2Client.class, projectContext);
    } catch (FileNotFoundException e) {
      logger.error(
          "Could not find config file: " + configFilename + " for context " + projectContext);
    } catch (JAXBException | SAXException | ParserConfigurationException e) {
      logger.error("Could not read config file");
    }
    return oauthClient;
  }

}
