/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.share.common.utils;

import de.samply.config.util.FileFinderUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class AbstractConfig.
 */
public abstract class AbstractConfig {

  /**
   * The logger.
   */
  private static final Logger logger = LoggerFactory.getLogger(AbstractConfig.class);
  /**
   * The props.
   */
  private Properties props;
  /**
   * The error msg.
   */
  private String errorMsg = null;
  /**
   * The config path.
   */
  private String configPath = null;
  /**
   * The file name of the config file
   */
  private String configFilename;

  /**
   * Instantiates a new config.
   *
   * @param configFilename name of the config file
   */
  protected AbstractConfig(String configFilename) {
    this.configFilename = configFilename;
    props = new Properties();

    try {
      File file = FileFinderUtil
          .findFile(configFilename, ProjectInfo.INSTANCE.getProjectName(), null);

      logger.info("Reading config from file " + file.getAbsolutePath());

      FileInputStream configInputStream = new FileInputStream(file);

      props.load(configInputStream);
      configInputStream.close();
      logger.info("Config read successfully");

    } catch (IOException e) {
      String error = "Error reading configuration file: " + e.getMessage();
      logger.error(error, e);
      errorMsg = error;
      throw new RuntimeException(errorMsg);
    }
  }

  /**
   * Gets the error msg.
   *
   * @return the error msg
   */
  public String getErrorMsg() {
    return errorMsg;
  }

  /**
   * Gets the properties.
   *
   * @return the properties
   */
  public Properties getProperties() {
    return props;
  }

  /**
   * Gets the property.
   *
   * @param propKey the prop key
   * @return the property
   */
  public String getProperty(String propKey) {
    return props.getProperty(propKey);
  }

  /**
   * Check whether debug is on
   *
   * @return true, if successful
   */
  public boolean debugIsOn() {
    String debugMode = props.getProperty("debug");
    return (debugMode != null && debugMode.equalsIgnoreCase("true"));
  }

  protected void setProperty(String propKey, String val) {
    props.setProperty(propKey, val);
  }

  public String getConfigFileName() {
    return configFilename;
  }

  /**
   * Gets the config path.
   *
   * @return the config path
   */
  public String getConfigPath() {

    if (configPath == null) {
      try {
        configPath = FileFinderUtil.findFile(getConfigFileName(),
            ProjectInfo.INSTANCE.getProjectName(), null).getParent();
      } catch (FileNotFoundException e) {
        logger.error("Could not find config file...");
        e.printStackTrace();
      }
    }
    return configPath;
  }
}
