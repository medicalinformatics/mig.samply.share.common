/*
 * Copyright (C) 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.share.common.utils;

import java.util.regex.PatternSyntaxException;

public class MdrIdDatatype {

  public static final String SEPARATOR = ":";
  public static final String LATEST_VERSION_WILDCARD_MDR = "latest";
  public static final String LATEST_VERSION_WILDCARD_CENTRAXX = "*";
  private static final String URN = "urn";

  private String namespace;
  private String datatype;
  private String id;
  private String version;

  public MdrIdDatatype(String urn) {
    if (!urn.startsWith(URN)) {
      throw new IllegalArgumentException("wrong prefix: " + urn);
    }
    try {
      String[] urnTokens = urn.split(SEPARATOR);
      if (urnTokens.length < 4 || urnTokens.length > 5) {
        throw new IllegalArgumentException("Invalid amount of tokens: " + urn);
      }
      this.namespace = urnTokens[1];
      this.datatype = urnTokens[2];
      this.id = urnTokens[3];
      if (urnTokens.length > 4) {
        this.version = urnTokens[4];
      } else {
        this.version = "";
      }
    } catch (PatternSyntaxException pse) {
      throw new IllegalArgumentException("Pattern Syntax Exception.", pse);
    } catch (NumberFormatException nfe) {
      throw new IllegalArgumentException("Number Format Exception.", nfe);
    }
  }

  public String getNamespace() {
    return namespace;
  }

  public void setNamespace(String namespace) {
    this.namespace = namespace;
  }

  public String getDatatype() {
    return datatype;
  }

  public void setDatatype(String datatype) {
    this.datatype = datatype;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public String getMajor() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(URN).append(SEPARATOR)
        .append(namespace).append(SEPARATOR)
        .append(datatype).append(SEPARATOR)
        .append(id);

    return stringBuilder.toString();
  }

  public String getLatestMdr() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(URN).append(SEPARATOR)
        .append(namespace).append(SEPARATOR)
        .append(datatype).append(SEPARATOR)
        .append(id).append(SEPARATOR)
        .append(LATEST_VERSION_WILDCARD_MDR);

    return stringBuilder.toString();
  }

  public String getLatestCentraxx() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(URN).append(SEPARATOR)
        .append(namespace).append(SEPARATOR)
        .append(datatype).append(SEPARATOR)
        .append(id).append(SEPARATOR)
        .append(LATEST_VERSION_WILDCARD_CENTRAXX);

    return stringBuilder.toString();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    MdrIdDatatype that = (MdrIdDatatype) o;

    if (!namespace.equals(that.namespace)) {
      return false;
    }
    if (!datatype.equals(that.datatype)) {
      return false;
    }
    if (!id.equals(that.id)) {
      return false;
    }
    return version != null ? version.equals(that.version) : that.version == null;
  }

  public boolean equalsIgnoreVersion(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    MdrIdDatatype that = (MdrIdDatatype) o;

    if (!namespace.equals(that.namespace)) {
      return false;
    }
    if (!datatype.equals(that.datatype)) {
      return false;
    }
    return id.equals(that.id);
  }

  @Override
  public int hashCode() {
    int result = namespace.hashCode();
    result = 31 * result + datatype.hashCode();
    result = 31 * result + id.hashCode();
    result = 31 * result + (version != null ? version.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(URN).append(SEPARATOR)
        .append(namespace).append(SEPARATOR)
        .append(datatype).append(SEPARATOR)
        .append(id).append(SEPARATOR)
        .append(version);

    return stringBuilder.toString();
  }

}
