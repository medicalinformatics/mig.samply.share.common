/*
 * Copyright (C) 2017 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.share.common.model.dto.monitoring;

public class StatusReportItem {

  public static final String PARAMETER_LDM_STATUS = "bridgehead-ldm";
  public static final String PARAMETER_LDM_VERSION = "bridgehead-ldm-version";
  public static final String PARAMETER_IDM_STATUS = "bridgehead-idm";
  public static final String PARAMETER_IDM_VERSION = "bridgehead-idm-version";
  public static final String PARAMETER_SHARE_STATUS = "bridgehead-teiler";
  public static final String PARAMETER_SHARE_VERSION = "bridgehead-teiler-version";
  public static final String PARAMETER_REFERENCE_QUERY_RESULTCOUNT = "bridgehead-referencequery-resultcount";
  public static final String PARAMETER_REFERENCE_QUERY_RUNTIME = "bridgehead-referencequery-duration";
  public static final String PARAMETER_PATIENTS_TOTAL_COUNT = "bridgehead-patients-total";
  public static final String PARAMETER_PATIENTS_DKTKFLAGGED_COUNT = "bridgehead-patients-dktkflagged";
  public static final String PARAMETER_CENTRAXX_MAPPING_VERSION = "centraxx-mapping-version";
  public static final String PARAMETER_CENTRAXX_MAPPING_DATE = "centraxx-mapping-date";

  private String parameter_name;
  private String status_text;
  private String exit_status;

  public String getParameter_name() {
    return parameter_name;
  }

  public void setParameter_name(String parameter_name) {
    this.parameter_name = parameter_name;
  }

  public String getStatus_text() {
    return status_text;
  }

  public void setStatus_text(String status_text) {
    this.status_text = status_text;
  }

  public String getExit_status() {
    return exit_status;
  }

  public void setExit_status(String exit_status) {
    this.exit_status = exit_status;
  }
}
