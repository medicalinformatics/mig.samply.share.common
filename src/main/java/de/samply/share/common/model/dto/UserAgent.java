/*
 * Copyright (C) 2017 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.share.common.model.dto;

/**
 * Information about the user agent
 */
public class UserAgent {

  private String projectContext;
  private String shareName;
  private String shareVersion;
  private String ldmName;
  private String ldmVersion;
  private String idManagerName;
  private String idManagerVersion;

  /**
   * Construct user agent object with all parameters supplied
   *
   * @param projectContext the name of the project
   * @param shareName the name of the share client
   * @param shareVersion the version of the share client
   * @param ldmName the name of the attached local datamanagement
   * @param ldmVersion the version of the attached local datamanagement
   * @param idManagerName the name of the attached id management
   * @param idManagerVersion the version of the attached id management
   */
  public UserAgent(String projectContext, String shareName, String shareVersion, String ldmName,
      String ldmVersion,
      String idManagerName, String idManagerVersion) {
    this.projectContext = projectContext;
    this.shareName = shareName;
    this.shareVersion = shareVersion;
    this.ldmName = ldmName;
    this.ldmVersion = ldmVersion;
    this.idManagerName = idManagerName;
    this.idManagerVersion = idManagerVersion;
  }

  /**
   * Construct user agent object with only the parameters from share client supplied
   *
   * @param projectContext the name of the project
   * @param shareName the name of the share client
   * @param shareVersion the version of the share client
   */
  public UserAgent(String projectContext, String shareName, String shareVersion) {
    this.projectContext = projectContext;
    this.shareName = shareName;
    this.shareVersion = shareVersion;
  }

  /**
   * Construct user agent object from user agent string
   *
   * @param userAgentString a complete user agent string
   */
  public UserAgent(String userAgentString) {
    if (userAgentString == null) {
      this.projectContext = "unknown";
      this.shareName = "unknown";
      this.shareVersion = "unknown";
      return;
    }
    int ldmStart = userAgentString.indexOf("ldm=");
    int idmStart = userAgentString.indexOf("idm=");
    int ldmSlash = ldmStart > 0 ? userAgentString.indexOf('/', ldmStart) : -1;
    int idmSlash = idmStart > 0 ? userAgentString.indexOf('/', idmStart) : -1;
    int shareSlash = userAgentString.indexOf('/');
    int projectBegin = userAgentString.indexOf('(');
    int projectEnd = userAgentString.indexOf(')', projectBegin);

    if (shareSlash < 0) {
      this.projectContext = "unknown";
      this.shareName = "unknown";
      this.shareVersion = "unknown";
      return;
    }

    this.shareName = userAgentString.substring(0, shareSlash);
    int shareEnd = userAgentString.indexOf(' ', shareSlash);
    if (shareEnd > 0) {
      this.shareVersion = userAgentString.substring(shareSlash + 1, shareEnd);
    } else {
      this.shareVersion = userAgentString.substring(shareSlash + 1);
    }
    if (projectBegin > 0 && projectEnd > 0) {
      this.projectContext = userAgentString.substring(projectBegin + 1, projectEnd);
    }
    if (ldmStart > 0) {
      this.ldmName = userAgentString.substring(ldmStart + 4, ldmSlash);
      int ldmEnd = userAgentString.indexOf(' ', ldmSlash);
      if (ldmEnd > 0) {
        this.ldmVersion = userAgentString.substring(ldmSlash + 1, ldmEnd);
      } else {
        this.ldmVersion = userAgentString.substring(ldmSlash + 1);
      }
    }
    if (idmStart > 0) {
      this.idManagerName = userAgentString.substring(idmStart + 4, idmSlash);
      int idmEnd = userAgentString.indexOf(' ', idmSlash);
      if (idmEnd > 0) {
        this.idManagerVersion = userAgentString.substring(idmSlash + 1, idmEnd);
      } else {
        this.idManagerVersion = userAgentString.substring(idmSlash + 1);
      }
    }
  }

  public String getProjectContext() {
    return projectContext;
  }

  public void setProjectContext(String projectContext) {
    this.projectContext = projectContext;
  }

  public String getShareName() {
    return shareName;
  }

  public void setShareName(String shareName) {
    this.shareName = shareName;
  }

  public String getShareVersion() {
    return shareVersion;
  }

  public void setShareVersion(String shareVersion) {
    this.shareVersion = shareVersion;
  }

  public String getLdmName() {
    return ldmName;
  }

  public void setLdmName(String ldmName) {
    this.ldmName = ldmName;
  }

  public String getLdmVersion() {
    return ldmVersion;
  }

  public void setLdmVersion(String ldmVersion) {
    this.ldmVersion = ldmVersion;
  }

  public String getIdManagerName() {
    return idManagerName;
  }

  public void setIdManagerName(String idManagerName) {
    this.idManagerName = idManagerName;
  }

  public String getIdManagerVersion() {
    return idManagerVersion;
  }

  public void setIdManagerVersion(String idManagerVersion) {
    this.idManagerVersion = idManagerVersion;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    if (shareName != null) {
      builder.append(shareName);
    } else {
      builder.append("unknownClient");
    }
    builder.append("/");
    if (shareVersion != null) {
      builder.append(shareVersion);
    } else {
      builder.append("unknown");
    }

    if (projectContext != null) {
      builder.append(" (");
      builder.append(projectContext);
      builder.append(")");
    }

    if (ldmName != null || ldmVersion != null) {
      builder.append(" ldm=");
      if (ldmName != null) {
        builder.append(ldmName);
      } else {
        builder.append("unknown");
      }
      builder.append("/");
      if (ldmVersion != null) {
        builder.append(ldmVersion);
      } else {
        builder.append("unknown");
      }
    }

    if (idManagerName != null || idManagerVersion != null) {
      builder.append(" idm=");
      if (idManagerName != null) {
        builder.append(idManagerName);
      } else {
        builder.append("unknown");
      }
      builder.append("/");
      if (idManagerVersion != null) {
        builder.append(idManagerVersion);
      } else {
        builder.append("unknown");
      }
    }
    return builder.toString();
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((idManagerName == null) ? 0 : idManagerName.hashCode());
    result = prime * result + ((idManagerVersion == null) ? 0 : idManagerVersion.hashCode());
    result = prime * result + ((ldmName == null) ? 0 : ldmName.hashCode());
    result = prime * result + ((ldmVersion == null) ? 0 : ldmVersion.hashCode());
    result = prime * result + ((projectContext == null) ? 0 : projectContext.hashCode());
    result = prime * result + ((shareName == null) ? 0 : shareName.hashCode());
    result = prime * result + ((shareVersion == null) ? 0 : shareVersion.hashCode());
    return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    UserAgent other = (UserAgent) obj;
    if (idManagerName == null) {
      if (other.idManagerName != null) {
        return false;
      }
    } else if (!idManagerName.equals(other.idManagerName)) {
      return false;
    }
    if (idManagerVersion == null) {
      if (other.idManagerVersion != null) {
        return false;
      }
    } else if (!idManagerVersion.equals(other.idManagerVersion)) {
      return false;
    }
    if (ldmName == null) {
      if (other.ldmName != null) {
        return false;
      }
    } else if (!ldmName.equals(other.ldmName)) {
      return false;
    }
    if (ldmVersion == null) {
      if (other.ldmVersion != null) {
        return false;
      }
    } else if (!ldmVersion.equals(other.ldmVersion)) {
      return false;
    }
    if (projectContext == null) {
      if (other.projectContext != null) {
        return false;
      }
    } else if (!projectContext.equals(other.projectContext)) {
      return false;
    }
    if (shareName == null) {
      if (other.shareName != null) {
        return false;
      }
    } else if (!shareName.equals(other.shareName)) {
      return false;
    }
    if (shareVersion == null) {
      return other.shareVersion == null;
    } else
      return shareVersion.equals(other.shareVersion);
  }
}
