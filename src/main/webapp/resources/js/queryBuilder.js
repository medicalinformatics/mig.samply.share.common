$(document).ready(function () {

  const ops = ["IS_NULL", "IS_NOT_NULL"];
  $('#searchIntro').on('hide.bs.collapse', function () {
    $(this).find('i').removeClass('fa-remove').addClass('fa-question');
  });
  $('#searchIntro').on('show.bs.collapse', function () {
    $(this).find('i').removeClass('fa-question').addClass('fa-remove');
  });

  $(".query-builder-panel").on("change", ".operatorSelect", function () {
    let operator = $(this).val();
    $('.saved').hide();
    if ($.inArray(operator, ops) > -1) {
      $(this).parentsUntil('.row').parent().find('.values :input').prop(
          'disabled', true);
      $(this).parentsUntil('.row').parent().find('.values').hide();
    } else {
      $(this).parentsUntil('.row').parent().find('.values :input').prop(
          'disabled', false);
      $(this).parentsUntil('.row').parent().find('.values').show();
    }
    if (operator == "BETWEEN") {
      $(this).parentsUntil('.row').parent().find('.valuesSingle').hide();
      $(this).parentsUntil('.row').parent().find('.valuesSingle :input').prop(
          'disabled', true);
      $(this).parentsUntil('.row').parent().find('.valuesBetween').show();
      $(this).parentsUntil('.row').parent().find('.valuesBetween :input').prop(
          'disabled', false);
    } else {
      $(this).parentsUntil('.row').parent().find('.valuesBetween').hide();
      $(this).parentsUntil('.row').parent().find('.valuesBetween :input').prop(
          'disabled', true);
      $(this).parentsUntil('.row').parent().find('.valuesSingle').show();
      $(this).parentsUntil('.row').parent().find('.valuesSingle :input').prop(
          'disabled', false);
    }
  });

  makePanelsDroppable();

  $('.project-panel').click(function () {
    $(this).find('.go-to-project a').trigger("click");
  });

});

function makePanelsDroppable() {
  $(".sortableItemPanel .conjunctionGroupItem").droppable({
    accept: '.draggableItem',
    greedy: true,
    activeClass: 'animated pulse droppableActive',
    hoverClass: 'droppableHover',
    drop: function (event, ui) {
      $('.saved').hide();
      $('div.sortableItemPanel').block(
          {message: null, overlayCSS: {opacity: 0.5}});
      let mdrid = $(ui.draggable).find('#mdrId').val();
      let tmpid = $(this).find('#tempId').val();
      let searchString = $(ui.draggable).find('#searchString').val();

      if (typeof (searchString) === "undefined") {
        searchString = "";
      }

      if (typeof (tmpid) === "undefined") {
        addItemToQueryRoot({'mdrId': mdrid, 'searchString': searchString});
      } else {
        addItemToQueryGroup(
            {'mdrId': mdrid, 'searchString': searchString, 'tmpId': tmpid});
      }
    }
  });
}