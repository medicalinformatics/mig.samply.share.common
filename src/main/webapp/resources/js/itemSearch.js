$(document).ready(function () {
  $(".itemSelectBox").select2({
    placeholder: "Keyword",
    allowClear: true,
    maximumSelectionSize: 4
  });

  $('.multiLevelMenu').on('click', '.collapse.in', function () {
    $(this).removeClass('in');
  });

  createPopovers();
});

// on enter pressed, execute search
$('.itemSearchPanel').keypress(function (e) {
  if (e.key === 'Enter') {
    $('.searchButton').click();
    e.stopImmediatePropagation();
    e.preventDefault();
    return false;
  }
});

function onItemSearchExpand(data) {
  if (data.status == "begin") {
    $('.itemSearchPanel').block(
        {message: $('.loader'), overlayCSS: {opacity: 0.5}});
  } else if (data.status == "complete") {
    $('.itemSearchPanel').unblock();
  }
};

function createPopovers() {
  $('[data-toggle="popover"]').popover({
    html: true,
    delay: {"show": 500, "hide": 100},
    trigger: 'hover'
  });
};