# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.5.0] - 2019-09-26
### Added
- Update mdrfaces

## [3.4.0] - 2019-09-02
### Added
- Constants for replies without count

## [3.3.0] - 2019-08-30
### Added
- Block the item search panel and display a spinner animation when searching
### Fixed
- The "like" and "not equals" operators are available for catalogue selections
- records will be expanded to their members in mdr item search

## [3.2.0] - 2019-08-07
### Fixed
- Hide refresh button when filtered mdr item list is shown
### Changed
- Update to mdrfaces with searchable catalogs
- Move catalogue modal outside of omnifaces tree

## [3.1.0] 2019-06-19
### Removed
- Moved QueryConverter functions to share.dto

### Fixed
- Don't try to reformat "is null" or "is not null" nodes
- Fix crash when "between" operator is present
- Change use of log4j to slf4j in QueryValidator
- Add config filename as parameter to get logout url

### Changed
- Update mdrfaces to a version that supports lazy loading
- Leave font-weight untouched for form Items. Increases readability
- Remove List style from search results
- Only redirect to ADFS logout when "DKFZ" is configured as external identity provider
- Moved functions dealing with query <-> tree conversion to QueryTreeUtil class
- Throw Parse Exception when trying to convert dates and have the client application handle it
- Throw Parse Exception when trying to convert dates and have the client application handle it
- Upgrade to auth-dto 3.0.0
- Change visibility for methods QueryValidator.getExpandedDateValue and QueryValidator.getReformattedDateValue to public
and rename those methods to avoid misunderstandings
- Upgrade to share dto 3.0.0
- Switch to common namespace
- Upgrade to mdrfaces/client with jersey2
- Use samply auth client
- Set default redirect uri to /user/dashboard

### Added
- Add Centraxx Mapping Date Parameter to StatusReportItem Class
- Add Centraxx Mapping Version Parameter to StatusReportItem Class
- Utils class, collecting utility methods from share client and broker
- Paths to monitoring added to constants
- CHANGELOG.md
- Add Method to get a list of mdr dataelements
- Add the option to add a fixed set of items to include in a search
- Optional button for select viewfields action
- method to safely combine urls and paths
- Autodetect date format and convert it to validation format

## [2.0.0] 2019-01-11
### Added
- Initial release
